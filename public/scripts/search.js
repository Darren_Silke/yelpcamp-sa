$("#campground-search").on("input", function(){
	var search = $(this).serialize();
	if (search === "search=") {
		search = "all";
	}
	$.get("/campgrounds?" + search, function(data){
		if (data.campgrounds.length > 0) {
			$("#campground-grid").html("");
			data.campgrounds.forEach(function(campground){
				var htmlToAppend = ``;
				htmlToAppend +=
				`
					<div class="col-md-3 col-sm-6">
						<div class="thumbnail">
							<img src="${campground.image}" alt="Campground image">
							<div class="caption">
								<h4 class="min-height">${campground.name}</h4>
				`;
				if (campground.rating === 0) {
					htmlToAppend += `<em class="grey">No reviews yet</em>`;
				} else {
					htmlToAppend +=
					`
						<span class="fas fa-star orange" aria-hidden="true"></span>
						<span class="fas fa-star
					`;
					if (campground.rating >= 1.5) {
						htmlToAppend += ` orange`;
					}
					htmlToAppend +=
					`
						" aria-hidden="true"></span>
						<span class="fas fa-star
					`;
					if (campground.rating >= 2.5) {
						htmlToAppend += ` orange`;
					}
					htmlToAppend +=
					`
						" aria-hidden="true"></span>
						<span class="fas fa-star
					`;
					if (campground.rating >= 3.5) {
						htmlToAppend += ` orange`;
					}
					htmlToAppend +=
					`
						" aria-hidden="true"></span>
						<span class="fas fa-star
					`;
					if (campground.rating >= 4.5) {
						htmlToAppend += ` orange`;
					}
					var starOrStars = campground.rating.toFixed(0) === "1" ? "star" : "stars";
					htmlToAppend +=
					`
						" aria-hidden="true"></span>
						<span class="sr-only">Rating: ${campground.rating.toFixed(0)} ${starOrStars} out of 5 stars</span>
					`;
				}
				htmlToAppend +=
				`
								<div>
									<span class="sr-only">Total likes:</span>
									<span class="badge label-primary"><i class="fas fa-thumbs-up" aria-hidden="true"></i> ${campground.likes.length}</span>
								</div>
							</div>
							<p>
								<a class="btn btn-primary" href="/campgrounds/${campground.slug}" role="button">More Info</a>
							</p>
						</div>
					</div>
				`;
				$("#campground-grid").append(htmlToAppend);
			});

			$("#campground-pagination").html("");
			if (search === "all") {
				if (data.pages && data.pages > 0 && data.campgrounds.length > 0) {
					var htmlToAppend = ``;
					htmlToAppend += `<ul class="pagination pagination-sm">`;

					if (data.current === 1) {
						htmlToAppend += `<li class="disabled"><a>First</a></li>`;
					} else {
						htmlToAppend += `<li><a href="/campgrounds">First</a></li>`;
					}

					if (data.current === 1) {
						htmlToAppend += `<li class="disabled"><a>«</a></li>`;
					} else {
						htmlToAppend += `<li><a href="/campgrounds?page=${data.current - 1}">«</a></li>`;
					}

					var i = data.current > 2 ? data.current - 1 : 1;
					if (i !== 1) {
						htmlToAppend += `<li class="disabled"><a>...</a></li>`;
					}
					for (; i <= data.current + 1 && i <= data.pages; i++) {
						if (i === data.current) {
							htmlToAppend += `<li class="active"><a>${i}</a></li>`;
						} else {
							htmlToAppend += `<li><a href="/campgrounds?page=${i}">${i}</a></li>`;
						}
						if (i === data.current + 1 && i < data.pages) {
							htmlToAppend += `<li class="disabled"><a>...</a></li>`;
						}
					}

					if (data.current === data.pages) {
						htmlToAppend += `<li class="disabled"><a>»</a></li>`;
					} else {
						htmlToAppend += `<li><a href="/campgrounds?page=${data.current + 1}">»</a></li>`;
					}

					if (data.current === data.pages) {
						htmlToAppend += `<li class="disabled"><a>Last</a></li>`;
					} else {
						htmlToAppend += `<li><a href="/campgrounds?page=${data.pages}">Last</a></li>`;
					}

					htmlToAppend += `</ul>`;

					$("#campground-pagination").append(htmlToAppend);
				}
			}
		} else {
			$("#campground-grid").html("");
			$("#campground-pagination").html("");
			var htmlToAppend =
			`
				<div class="col-xs-12">
					<h2><em class="grey">No campgrounds match your search query</em></h2>
				</div>
			`;
			$("#campground-grid").append(htmlToAppend);
		}
	});
});

$("#campground-search").submit(function(event){
	event.preventDefault();
});

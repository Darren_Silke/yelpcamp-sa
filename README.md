# YelpCamp SA

YelpCamp SA is a Web application, where users can add their own campgrounds, view campgrounds, leave comments on campgrounds and provide reviews for campgrounds. The idea is to provide a crowd-sourced library of the best campgrounds from all over South Africa. Users can also like campgrounds (similar to Facebook's like button), and can follow other users to receive notifications when new campgrounds are added.

A live version of YelpCamp SA can be viewed [here](https://yelpcampsa.herokuapp.com).

## Installation

To run locally, YelpCamp SA requires [Node.js](https://nodejs.org) to run.

The following additional prerequisites are required in order for YelpCamp SA to run properly:

* [MongoDB](https://www.mongodb.com)
* [Google Maps Platform](https://cloud.google.com/maps-platform)
* [SendGrid](https://sendgrid.com)
* [Cloudinary](https://cloudinary.com)

A '.env' file for environment variables used by YelpCamp SA must be created in the root directory of YelpCamp SA and should be configured as follows:

*DATABASE_URL=mongodb://127.0.0.1:27017/yelpcamp-sa*  
*PORT=3000*  
*IP=localhost*  
*ENVIRONMENT=development*  
*GEOCODER_API_KEY=YOUR_GEOCODER_API_KEY*  
*SENDGRID_API_KEY=YOUR_SENDGRID_API_KEY*  
*SENDGRID_VERIFIED_EMAIL_ADDRESS=YOUR_SENDGRID_VERIFIED_EMAIL_ADDRESS*  
*CLOUDINARY_API_KEY=YOUR_CLOUDINARY_API_KEY*  
*CLOUDINARY_API_SECRET=YOUR_CLOUDINARY_API_SECRET*  
*ADMIN_CODE=YOUR_ADMIN_CODE*  
*EXPRESS_SESSION_SECRET=YOUR_EXPRESS_SESSION_SECRET*

'DATABASE_URL' refers to the URL for your local MongoDB instance.

'PORT' refers to the port that YelpCamp SA will be available on.

'IP' refers to the IP address that YelpCamp SA will be available at.

'ENVIRONMENT' refers to the environment that YelpCamp SA will run on. If set to 'development', YelpCamp SA will run over http, and if set to 'production', YelpCamp SA will run over https (SSL needs to be configured for this to work).

'GEOCODER_API_KEY' refers to your Geocoder API key from Google Maps Platform.

'SENDGRID_API_KEY' refers to your SendGrid API key from SendGrid.

'SENDGRID_VERIFIED_EMAIL_ADDRESS' refers to the email address from where emails will be sent from. The email address is required to be verified by SendGrid. See [here](https://docs.sendgrid.com/ui/sending-email/sender-verification) for further information.

'CLOUDINARY_API_KEY' refers to your Cloudinary API key from Cloudinary.

'CLOUDINARY_API_SECRET' refers to your Cloudinary API secret from Cloudinary.

'ADMIN_CODE' refers to the admin code a user enters for admin/superuser access when signing up for a YelpCamp SA account. This can be set to anything.

'EXPRESS_SESSION_SECRET' refers to a password/phrase that express-session uses. This can be set to anything.

Once the above prerequisites have been met, and the various environment variables set, YelpCamp SA can be run by installing the dependencies and starting the server. The below commands must be run from the root directory of YelpCamp SA.

```sh
$ npm install
$ npm start
```

To stop the server, press `ctrl+c`

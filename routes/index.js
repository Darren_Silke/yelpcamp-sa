var express      = require("express");
var router       = express.Router();
var passport     = require("passport");
var User         = require("../models/user");
var Notification = require("../models/notification");
var async        = require("async");
var nodemailer   = require("nodemailer");
var crypto       = require("crypto");
var middleware   = require("../middleware");

// Index/landing page (GET)
router.get("/", function(req, res){
	res.render("landing");
});

// Register a user (GET)
router.get("/register", function(req, res){
	res.render("register", {page: "register"});
});

// Register a user (POST)
router.post("/register", function(req, res){
	if (req.body.password === req.body.confirm) {
		var newUser = new User({
			username: req.body.username,
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			email: req.body.email.toLowerCase()
		});

		if (req.body.adminCode === process.env.ADMIN_CODE) {
			newUser.isAdmin = true;
		}

		User.register(newUser, req.body.password, function(err, user){
			if (err) {
				console.error(err);
				var errorMessage = err.message;
				if (errorMessage.includes("E11000 duplicate key error collection:")) {
					errorMessage = "A user with the given email address is already registered";
				}
				return res.render("register", {error: errorMessage});
			}
			passport.authenticate("local")(req, res, function(){
				req.flash("success", "Successfully signed up! Welcome to YelpCamp SA " + req.body.firstName + "!");
				res.redirect("/campgrounds");
			});
		});
	} else {
		req.flash("error", "Passwords do not match");
		res.redirect("back");
	}
});

// Log in a user (GET)
router.get("/login", function(req, res){
	res.render("login", {page: "login"});
});

// Log in a user (POST)
router.post("/login", passport.authenticate("local",
	{
		successRedirect: "/campgrounds",
		failureRedirect: "/login",
		failureFlash: true,
		successFlash: "Welcome back!"
	}), function(req, res){
});

// Log out a user (GET)
router.get("/logout", function(req, res){
	req.logout();
	req.flash("success", "See you later!");
	res.redirect("/campgrounds");
});

// Handle notification (GET)
router.get("/notifications/:id", middleware.isLoggedIn, function(req, res){
	Notification.findById(req.params.id, function(err, notification){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		} else if (!notification) {
			req.flash("error", "Sorry, that notification does not exist");
			return res.redirect("back");
		}
		User.findByIdAndUpdate(req.user._id, {
			$pull: {
				notifications: notification._id
			}
		}, function(err, user){
			if (err) {
				console.error(err);
				req.flash("error", "Oops! It looks like something went wrong!");
				return res.redirect("back");
			} else if (!user.notifications.includes(notification._id)) {
				req.flash("error", "Sorry, that notification does not belong to you");
				return res.redirect("back");
			}
			notification.remove(function(err){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				}
				res.redirect("/campgrounds/" + notification.campgroundSlug);
			});
		});
	});
});

// Forgot password (GET)
router.get("/forgot", function(req, res){
	res.render("forgot");
});

// Forgot password (POST)
router.post("/forgot", function(req, res){
	if (req.body.email === "") {
		req.flash("error", "No email address was given");
		return res.redirect("back");
	}
	async.waterfall([
		function(done){
			crypto.randomBytes(20, function(err, buf){
				var token = buf.toString("hex");
				done(err, token);
			});
		},
		function(token, done){
			User.findOne({email: req.body.email.toLowerCase()}, function(err, user){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				} else if (!user) {
					req.flash("error", "No account with that email address exists");
					return res.redirect("back");
				}

				user.resetPasswordToken = token;
				user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

				user.save(function(err){
					done(err, token, user);
				});
			});
		},
		function(token, user, done){
			var smtpTransport = nodemailer.createTransport({
				service: "SendGrid",
				auth: {
					user: "apikey",
					pass: process.env.SENDGRID_API_KEY
				}
			});
			var protocol = process.env.ENVIRONMENT === "production" ? "https://" : "http://";
			var mailOptions = {
				to: user.email,
				from: "YelpCamp SA <" + process.env.SENDGRID_VERIFIED_EMAIL_ADDRESS + ">",
				subject: "Reset your YelpCamp SA password",
				text: "Hi " + user.firstName + " (username: " + user.username + "),\n\n" +
					"Someone recently requested a password change for your YelpCamp SA account. If this was you, you can set a new password here:\n\n" +
					protocol + req.headers.host + "/reset/" + token + "\n\n" +
					"If you don't want to change your password or didn't request this, just ignore and delete this message.\n\n" +
					"To keep your account secure, please don't forward this email to anyone.\n"
			};
			smtpTransport.sendMail(mailOptions, function(err){
				req.flash("success", "An email has been sent to " + user.email + " with further instructions. You may need to check your spam folder or unblock " + process.env.SENDGRID_VERIFIED_EMAIL_ADDRESS +".");
				done(err);
			});
		}
	], function(err){
		if (err) {
			console.error(err);
			req.session.flash = {};
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		}
		res.redirect("/forgot");
	});
});

// Reset password (GET)
router.get("/reset/:token", function(req, res){
	User.findOne({resetPasswordToken: req.params.token, resetPasswordExpires: {$gt: Date.now()}}, function(err, user){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("/forgot");
		} else if (!user) {
			req.flash("error", "Password reset token is invalid or has expired");
			return res.redirect("/forgot");
		}
		res.render("reset", {token: req.params.token});
	});
});

// Reset password (POST)
router.post("/reset/:token", function(req, res){
	async.waterfall([
		function(done){
			User.findOne({resetPasswordToken: req.params.token, resetPasswordExpires: {$gt: Date.now()}}, function(err, user){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("/forgot");
				} else if (!user) {
					req.flash("error", "Password reset token is invalid or has expired");
					return res.redirect("/forgot");
				}
				if (req.body.password === req.body.confirm) {
					if (req.body.password === "") {
						req.flash("error", "No password was given");
						return res.redirect("back");
					}
					user.setPassword(req.body.password, function(err){
						if (err) {
							console.error(err);
							req.flash("error", "Oops! It looks like something went wrong!");
							return res.redirect("/forgot");
						}
						user.resetPasswordToken = undefined;
						user.resetPasswordExpires = undefined;

						user.save(function(err){
							if (err) {
								console.error(err);
								req.flash("error", "Oops! It looks like something went wrong!");
								return res.redirect("/forgot");
							}
							req.login(user, function(err){
								done(err, user);
							});
						});
					});
				} else {
					req.flash("error", "Passwords do not match");
					return res.redirect("back");
				}
			});
		},
		function(user, done){
			var smtpTransport = nodemailer.createTransport({
				service: "SendGrid",
				auth: {
					user: "apikey",
					pass: process.env.SENDGRID_API_KEY
				}
			});
			var mailOptions = {
				to: user.email,
				from: "YelpCamp SA <" + process.env.SENDGRID_VERIFIED_EMAIL_ADDRESS + ">",
				subject: "Your YelpCamp SA password has been changed",
				text: "Hi " + user.firstName + " (username: " + user.username + "),\n\n" +
					"This is a confirmation message that the password for your YelpCamp SA account has just been changed.\n"
			};
			smtpTransport.sendMail(mailOptions, function(err){
				req.flash("success", "Password changed!");
				done(err);
			});
		}
	], function(err) {
		if (err) {
			console.error(err);
			req.logout();
			req.session.flash = {};
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("/forgot");
		}
		res.redirect("/campgrounds");
	});
});

module.exports = router;

var express      = require("express");
var router       = express.Router();
var Campground   = require("../models/campground");
var User         = require("../models/user");
var Notification = require("../models/notification");
var middleware   = require("../middleware");
var NodeGeocoder = require("node-geocoder");
var multer       = require("multer");
var cloudinary   = require("cloudinary");
var async        = require("async");

var storage = multer.diskStorage({
	filename: function(req, file, callback){
		callback(null, Date.now() + file.originalname);
	}
});

var imageFilter = function(req, file, callback) {
	// Accept image files only
	if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
		return callback(new Error("Only image files are allowed"), false);
	}
	callback(null, true);
};

var upload = multer({storage: storage, fileFilter: imageFilter});

cloudinary.config({
	cloud_name: "yelpcampsa",
	api_key: process.env.CLOUDINARY_API_KEY,
	api_secret: process.env.CLOUDINARY_API_SECRET
});

var options = {
	provider: "google",
	httpAdapter: "https",
	apiKey: process.env.GEOCODER_API_KEY,
	formatter: null
};

var geocoder = NodeGeocoder(options);

// Define escapeRegex function for search feature
function escapeRegex(text) {
	return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

// Index/home page to show all campgrounds (GET)
router.get("/", function(req, res){
	var perPage = 8;
	var pageQuery = parseInt(req.query.page);
	var pageNumber = pageQuery ? pageQuery : 1;
	if (req.query.search && req.xhr) {
		var regex = new RegExp(escapeRegex(req.query.search), "gi");
		// Get all campgrounds from database that match search query
		Campground.find({name: regex}, function(err, allCampgrounds){
			if (err) {
				console.error(err);
				res.redirect("/");
			} else {
				Campground.countDocuments({name: regex}).exec(function(err, count){
					if (err) {
						console.error(err);
						res.redirect("/");
					} else {
						res.status(200).json({
							campgrounds: allCampgrounds,
							current: pageNumber,
							pages: Math.ceil(count / perPage)
						});
					}
				});
			}
		});
	} else {
		// Get all campgrounds from database
		Campground.find({}).skip((perPage * pageNumber) - perPage).limit(perPage).exec(function(err, allCampgrounds){
			if (err) {
				console.error(err);
				res.redirect("/");
			} else {
				Campground.countDocuments({}).exec(function(err, count){
					if (err) {
						console.error(err);
						res.redirect("/");
					} else if (req.xhr) {
						res.status(200).json({
							campgrounds: allCampgrounds,
							current: pageNumber,
							pages: Math.ceil(count / perPage)
						});
					} else {
						res.render("campgrounds/index", {
							campgrounds: allCampgrounds,
							current: pageNumber,
							pages: Math.ceil(count / perPage),
							page: "campgrounds"
						});
					}
				});
			}
		});
	}
});

// Create a campground (GET)
router.get("/new", middleware.isLoggedIn, function(req, res){
	res.render("campgrounds/new");
});

// Create a campground (POST)
router.post("/", middleware.isLoggedIn, function(req, res){
	var uploadSingle = upload.single("image");
	uploadSingle(req, res, function(err){
		if (err) {
			console.error(err);
			req.flash("error", err.message);
			return res.redirect("back");
		}

		if (req.body.campground.name === "" || req.body.campground.price === "" || req.body.campground.description === "" || req.body.location === "") {
			req.flash("error", "Ensure that the name, price, description and location fields are filled in");
			return res.redirect("back");
		} else if (isNaN(req.body.campground.price)) {
			req.flash("error", "Price field only accepts numbers");
			return res.redirect("back");
		} else if (!req.file) {
			req.flash("error", "Select an image file to upload");
			return res.redirect("back");
		}

		geocoder.geocode(req.body.location, function(err, data){
			if (err) {
				console.error(err);
				req.flash("error", "Oops! It looks like something went wrong!");
				return res.redirect("back");
			} else if (!data.length) {
				req.flash("error", "Location is invalid");
				return res.redirect("back");
			}
			// Add latitude to campground
			req.body.campground.lat = data[0].latitude;
			// Add longitude to campground
			req.body.campground.lng = data[0].longitude;
			// Add location to campground
			req.body.campground.location = data[0].formattedAddress;

			cloudinary.v2.uploader.upload(req.file.path, async function(err, result){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				}
				// Split cloudinary URL string to allow for insertion of scaling parameters
				// All images will be scaled to have a width of 750 and a height of 500
				var splitString = result.secure_url.split("upload");
				// Add modified cloudinary URL for the image to the campground object under image property
				req.body.campground.image = splitString[0] + "upload/w_750,h_500,c_scale" + splitString[1];
				// Add image's public_id to campground object
				req.body.campground.imageId = result.public_id;

				// Add author to campground
				req.body.campground.author = {
					id: req.user._id,
					username: req.user.username
				};

				try {
					// Create a new campground and save to database
					// Get campground author's followers
					// For each follower, create a notification and save to database
					var campground = await Campground.create(req.body.campground);
					var user = await User.findById(req.user._id).populate("followers").exec();
					var newNotification = {
						username: req.user.username,
						campgroundSlug: campground.slug
					};
					for (var follower of user.followers) {
						var notification = await Notification.create(newNotification);
						follower.notifications.push(notification);
						await follower.save();
					}

					req.flash("success", "Added a new campground!");
					// Redirect to campground show page
					res.redirect("/campgrounds/" + campground.slug);
				} catch(err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					res.redirect("back");
				}
			});
		});
	});
});

// Update campground (GET)
router.get("/:slug/edit", middleware.isLoggedIn, middleware.checkCampgroundOwnership, function(req, res){
	// Render edit template with campground
	res.render("campgrounds/edit", {campground: req.campground});
});

// Update campground (PUT)
router.put("/:slug", middleware.isLoggedIn, middleware.checkCampgroundOwnership, function(req, res){
	var uploadSingle = upload.single("image");
	uploadSingle(req, res, function(err){
		if (err) {
			console.error(err);
			req.flash("error", err.message);
			return res.redirect("back");
		}

		if (req.body.campground.name === "" || req.body.campground.price === "" || req.body.campground.description === "" || req.body.location === "") {
			req.flash("error", "Ensure that the name, price, description and location fields are filled in");
			return res.redirect("back");
		} else if (isNaN(req.body.campground.price)) {
			req.flash("error", "Price field only accepts numbers");
			return res.redirect("back");
		}

		geocoder.geocode(req.body.location, function(err, data){
			if (err) {
				console.error(err);
				req.flash("error", "Oops! It looks like something went wrong!");
				return res.redirect("back");
			} else if (!data.length) {
				req.flash("error", "Location is invalid");
				return res.redirect("back");
			}

			// Find and update the correct campground
			Campground.findOne({slug: req.params.slug}, async function(err, campground){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				}

				if (req.file) {
					try {
						await cloudinary.v2.uploader.destroy(campground.imageId);
						var result = await cloudinary.v2.uploader.upload(req.file.path);
						// Add image's public_id to campground object
						campground.imageId = result.public_id;
						// Split cloudinary URL string to allow for insertion of scaling parameters
						// All images will be scaled to have a width of 750 and a height of 500
						var splitString = result.secure_url.split("upload");
						// Add modified cloudinary URL for the image to the campground object under image property
						campground.image = splitString[0] + "upload/w_750,h_500,c_scale" + splitString[1];
					} catch(err) {
						console.error(err);
						req.flash("error", "Oops! It looks like something went wrong!");
						return res.redirect("back");
					}
				}

				campground.name = req.body.campground.name;
				campground.price = req.body.campground.price;
				campground.description = req.body.campground.description;
				// Add latitude to campground
				campground.lat = data[0].latitude;
				// Add longitude to campground
				campground.lng = data[0].longitude;
				// Add location to campground
				campground.location = data[0].formattedAddress;
				try {
					await campground.save();
				} catch(err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				}

				req.flash("success", "Updated campground!");
				// Redirect to campground show page
				res.redirect("/campgrounds/" + campground.slug);
			});
		});
	});
});

// Delete campground (DELETE)
router.delete("/:slug", middleware.isLoggedIn, middleware.checkCampgroundOwnership, function(req, res){
	Campground.findOne({slug: req.params.slug}, async function(err, campground){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		}
		try {
			await cloudinary.v2.uploader.destroy(campground.imageId);
			await campground.remove();
		} catch(err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		}
		req.flash("success", "Deleted campground!");
		res.redirect("/campgrounds");
	});
});

// Show more info about one campground (GET)
router.get("/:slug", function(req, res){
	// Find the campground with provided slug
	Campground.findOne({slug: req.params.slug}).populate("comments likes").populate({
		path: "reviews",
		options: {sort: {createdAt: -1}} // Sort the populated reviews array to show the latest first
	}).exec(function(err, foundCampground){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		} else if (!foundCampground) {
			req.flash("error", "Sorry, that campground does not exist");
			return res.redirect("/campgrounds");
		}
		User.findById(foundCampground.author.id, function(err, foundUser){
			if (err) {
				console.error(err);
				req.flash("error", "Oops! It looks like something went wrong!");
				return res.redirect("back");
			}
			// Render show template with that campground
			res.render("campgrounds/show", {campground: foundCampground, user: foundUser});
		});
	});
});

// Campground like (POST)
router.post("/:slug/like", middleware.isLoggedIn, async function(req, res){
	try {
		var foundCampground = await Campground.findOne({slug: req.params.slug});
		if (!foundCampground) {
			req.flash("error", "Sorry, that campground does not exist");
			return res.redirect("/campgrounds");
		}

		// Check if req.user._id exists in foundCampground.likes
		var foundUserLike = foundCampground.likes.some(function(like){
			return like.equals(req.user._id);
		});

		var successMessage = "";
		if (foundUserLike) {
			// User already liked, remove like
			foundCampground.likes.pull(req.user._id);
			successMessage = "Unliked " + foundCampground.name + "!";
		} else {
			// Add the new user like
			foundCampground.likes.push(req.user._id);
			successMessage = "Liked " + foundCampground.name + "!";
		}

		await foundCampground.save();

		req.flash("success", successMessage);
		res.redirect("/campgrounds/" + foundCampground.slug);
	} catch(err) {
		console.error(err);
		req.flash("error", "Oops! It looks like something went wrong!");
		res.redirect("back");
	}
});

module.exports = router;

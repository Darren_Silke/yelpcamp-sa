var express    = require("express");
var router     = express.Router({mergeParams: true});
var middleware = require("../middleware");
var async      = require("async");
var User       = require("../models/user");

// User follow (POST)
router.post("/:id/follow", middleware.isLoggedIn, async function(req, res){
	try {
		var foundUser = await User.findById(req.params.id);
		if (!foundUser) {
			req.flash("error", "Sorry, that user does not exist");
			return res.redirect("/campgrounds");
		}

		// Check if req.user._id exists in foundUser.followers
		var foundUserFollower = foundUser.followers.some(function(follower){
			return follower.equals(req.user._id);
		});

		var successMessage = "";
		if (foundUserFollower) {
			// User already followed, remove follower
			foundUser.followers.pull(req.user._id);
			successMessage = "Unfollowed " + foundUser.username + "!";
		} else {
			// Add the new user follower
			foundUser.followers.push(req.user._id);
			successMessage = "Followed " + foundUser.username + "!";
		}

		await foundUser.save();

		req.flash("success", successMessage);
		res.redirect("/campgrounds/" + req.params.slug);
	} catch(err) {
		console.error(err);
		req.flash("error", "Oops! It looks like something went wrong!");
		res.redirect("back");
	}
});

module.exports = router;

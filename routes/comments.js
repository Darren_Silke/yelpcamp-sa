var express    = require("express");
var router     = express.Router({mergeParams: true});
var Campground = require("../models/campground");
var Comment    = require("../models/comment");
var middleware = require("../middleware");

// Create a comment (POST)
router.post("/", middleware.isLoggedIn, function(req, res){
	// Lookup campground using the slug
	Campground.findOne({slug: req.params.slug}, function(err, campground){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		} else if (!campground) {
			req.flash("error", "Sorry, that campground does not exist");
			return res.redirect("/campgrounds");
		}
		Comment.create(req.body.comment, function(err, comment){
			if (err) {
				console.error(err);
				req.flash("error", err.message);
				return res.redirect("back");
			}
			// Add author id and username to the comment
			comment.author.id = req.user._id;
			comment.author.username = req.user.username;
			// Save comment
			comment.save(function(err){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				}
				// Add comment to the campground comments array
				campground.comments.push(comment);
				// Save campground
				campground.save(function(err){
					if (err) {
						console.error(err);
						req.flash("error", "Oops! It looks like something went wrong!");
						return res.redirect("back");
					}
					req.flash("success", "Added a new comment!");
					res.redirect("/campgrounds/" + campground.slug);
				});
			});
		});
	});
});

// Update comment (PUT)
router.put("/:commentId", middleware.isLoggedIn, middleware.checkCommentOwnership, function(req, res){
	Comment.findByIdAndUpdate(req.params.commentId, req.body.comment, {runValidators: true}, function(err){
		if (err) {
			console.error(err);
			req.flash("error", err.message);
			return res.redirect("back");
		}
		req.flash("success", "Updated comment!");
		res.redirect("/campgrounds/" + req.params.slug);
	});
});

// Delete comment (DELETE)
router.delete("/:commentId", middleware.isLoggedIn, middleware.checkCommentOwnership, function(req, res){
	// Find campground, remove comment from comments array, delete comment in database
	Campground.findOneAndUpdate({slug: req.params.slug}, {
		$pull: {
			comments: req.comment._id
		}
	}, function(err, campground){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		} else if (!campground) {
			req.flash("error", "Sorry, that campground does not exist");
			return res.redirect("/campgrounds");
		} else if (!campground.comments.includes(req.comment._id)) {
			req.flash("error", "Sorry, that comment does not belong to this campground");
			return res.redirect("/campgrounds/" + req.params.slug);
		}
		req.comment.remove(function(err){
			if (err) {
				console.error(err);
				req.flash("error", "Oops! It looks like something went wrong!");
				return res.redirect("back");
			}
			req.flash("success", "Deleted comment!");
			res.redirect("/campgrounds/" + req.params.slug);
		});
	});
});

module.exports = router;

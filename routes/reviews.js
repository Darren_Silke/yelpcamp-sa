var express    = require("express");
var router     = express.Router({mergeParams: true});
var Campground = require("../models/campground");
var Review     = require("../models/review");
var middleware = require("../middleware");

// Review page to show all reviews (GET)
router.get("/", function(req, res){
	Campground.findOne({slug: req.params.slug}).populate({
		path: "reviews",
		options: {sort: {createdAt: -1}} // Sort the populated reviews array to show the latest first
	}).exec(function(err, campground){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		} else if (!campground) {
			req.flash("error", "Sorry, that campground does not exist");
			return res.redirect("/campgrounds");
		}
		res.render("reviews/index", {campground: campground});
	});
});

// Create a review (GET)
router.get("/new", middleware.isLoggedIn, middleware.checkReviewExistence, function(req, res){
	// middleware.checkReviewExistence checks if a user already reviewed the campground
	// Only one review per user is allowed
	res.render("reviews/new", {campground: req.campground});
});

// Create a review (POST)
router.post("/", middleware.isLoggedIn, middleware.checkReviewExistence, function(req, res){
	// middleware.checkReviewExistence checks if a user already reviewed the campground
	// Only one review per user is allowed
	Review.create(req.body.review, function(err, review){
		if (err) {
			console.error(err);
			req.flash("error", err.message);
			return res.redirect("back");
		}
		// Add author id, username and associated campground to the review
		review.author.id = req.user._id;
		review.author.username = req.user.username;
		review.campground = req.campground;
		// Save review
		review.save(function(err){
			if (err) {
				console.error(err);
				req.flash("error", "Oops! It looks like something went wrong!");
				return res.redirect("back");
			}
			req.campground.reviews.push(review);
			// Calculate the new average campground rating
			req.campground.rating = calculateAverageCampgroundRating(req.campground.reviews);
			// Save campground
			req.campground.save(function(err){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				}
				req.flash("success", "Added a new review!");
				res.redirect("/campgrounds/" + req.campground.slug);
			});
		});
	});
});

// Update review (GET)
router.get("/:reviewId/edit", middleware.isLoggedIn, middleware.checkReviewOwnership, function(req, res){
	res.render("reviews/edit", {review: req.review});
});

// Update review (PUT)
router.put("/:reviewId", middleware.isLoggedIn, middleware.checkReviewOwnership, function(req, res){
	Review.findByIdAndUpdate(req.params.reviewId, req.body.review, {runValidators: true}, function(err){
		if (err) {
			console.error(err);
			req.flash("error", err.message);
			return res.redirect("back");
		}
		Campground.findOne({slug: req.review.campground.slug}).populate("reviews").exec(function(err, campground){
			if (err) {
				console.error(err);
				req.flash("error", "Oops! It looks like something went wrong!");
				return res.redirect("back");
			} else if (!campground) {
				req.flash("error", "Sorry, that campground does not exist");
				return res.redirect("/campgrounds");
			}
			// Recalculate the average campground rating
			campground.rating = calculateAverageCampgroundRating(campground.reviews);
			// Save changes
			campground.save(function(err){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				}
				req.flash("success", "Updated review!");
				res.redirect("/campgrounds/" + campground.slug);
			});
		});
	});
});

// Delete review (DELETE)
router.delete("/:reviewId", middleware.isLoggedIn, middleware.checkReviewOwnership, function(req, res){
	// Find campground, remove review from reviews array, delete review in database
	Campground.findOneAndUpdate({slug: req.review.campground.slug}, {
		$pull: {
			reviews: req.review._id
		}
	}, {new: true}).populate("reviews").exec(function(err, campground){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			return res.redirect("back");
		} else if (!campground) {
			req.flash("error", "Sorry, that campground does not exist");
			return res.redirect("/campgrounds");
		}
		req.review.remove(function(err){
			if (err) {
				console.error(err);
				req.flash("error", "Oops! It looks like something went wrong!");
				return res.redirect("back");
			}
			// Recalculate the average campground rating
			campground.rating = calculateAverageCampgroundRating(campground.reviews);
			// Save changes
			campground.save(function(err){
				if (err) {
					console.error(err);
					req.flash("error", "Oops! It looks like something went wrong!");
					return res.redirect("back");
				}
				req.flash("success", "Deleted review!");
				res.redirect("/campgrounds/" + campground.slug);
			});
		});
	});
});

// Calculate the average campground rating
function calculateAverageCampgroundRating(reviews) {
	if (reviews.length === 0) {
		return 0;
	}
	var sum = 0;
	reviews.forEach(function(review){
		sum += review.rating;
	});
	return sum / reviews.length;
}

module.exports = router;

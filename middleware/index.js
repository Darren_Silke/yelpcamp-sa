var Campground = require("../models/campground");
var Comment    = require("../models/comment");
var Review     = require("../models/review");

// All the middleware goes here
var middlewareObj = {};

middlewareObj.isLoggedIn = function(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	req.flash("error", "You need to be logged in to do that");
	res.redirect("/login");
};

middlewareObj.checkCampgroundOwnership = function(req, res, next) {
	Campground.findOne({slug: req.params.slug}, function(err, foundCampground){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			res.redirect("/campgrounds");
		} else if (!foundCampground) {
			req.flash("error", "Sorry, that campground does not exist");
			res.redirect("/campgrounds");
		// Does user own the campground?
		} else if (foundCampground.author.id.equals(req.user._id) || req.user.isAdmin) {
			req.campground = foundCampground;
			next();
		} else {
			req.flash("error", "You don't have permission to do that");
			res.redirect("/campgrounds/" + req.params.slug);
		}
	});
};

middlewareObj.checkCommentOwnership = function(req, res, next) {
	Comment.findById(req.params.commentId, function(err, foundComment){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			res.redirect("/campgrounds");
		} else if (!foundComment) {
			req.flash("error", "Sorry, that comment does not exist");
			res.redirect("/campgrounds");
		// Does user own the comment?
		} else if (foundComment.author.id.equals(req.user._id) || req.user.isAdmin) {
			req.comment = foundComment;
			next();
		} else {
			req.flash("error", "You don't have permission to do that");
			res.redirect("/campgrounds/" + req.params.slug);
		}
	});
};

middlewareObj.checkReviewOwnership = function(req, res, next) {
	Review.findById(req.params.reviewId).populate("campground").exec(function(err, foundReview){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			res.redirect("/campgrounds");
		} else if (!foundReview) {
			req.flash("error", "Sorry, that review does not exist");
			res.redirect("/campgrounds");
		// Does user own the review?
		} else if (foundReview.author.id.equals(req.user._id) || req.user.isAdmin) {
			req.review = foundReview;
			next();
		} else {
			req.flash("error", "You don't have permission to do that");
			res.redirect("/campgrounds/" + req.params.slug);
		}
	});
};

middlewareObj.checkReviewExistence = function(req, res, next) {
	Campground.findOne({slug: req.params.slug}).populate("reviews").exec(function(err, foundCampground){
		if (err) {
			console.error(err);
			req.flash("error", "Oops! It looks like something went wrong!");
			res.redirect("/campgrounds");
		} else if (!foundCampground) {
			req.flash("error", "Sorry, that campground does not exist");
			res.redirect("/campgrounds");
		} else {
			// Check if req.user._id exists in foundCampground.reviews
			var foundUserReview = foundCampground.reviews.some(function(review){
				return review.author.id.equals(req.user._id);
			});
			if (foundUserReview) {
				req.flash("error", "You already wrote a review");
				return res.redirect("/campgrounds/" + req.params.slug);
			}
			// If the review was not found, continue
			req.campground = foundCampground;
			next();
		}
	});
};

module.exports = middlewareObj;

var mongoose = require("mongoose");

var reviewSchema = new mongoose.Schema({
	rating: {
		// Set the field type
		type: Number,
		// Make the star rating required
		required: "Provide a rating (1-5 stars)",
		// Define min and max values
		min: 1,
		max: 5,
		// Add validation to see if the entry is an integer
		validate: {
			// Validator accepts a function definition which it uses for validation
			validator: Number.isInteger,
			message: "{VALUE} is not an integer value"
		}
	},
	// Review text
	text: {
		type: String,
		required: "Review text cannot be blank"
	},
	// Author id and username fields
	author: {
		id: {
			type: mongoose.Schema.Types.ObjectId,
			ref: "User"
		},
		username: String
	},
	// Campground associated with the review
	campground: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Campground"
	}
}, {
	// If timestamps are set to true, mongoose assigns createdAt and updatedAt fields to the schema
	// The type assigned is Date
	timestamps: true
});

module.exports = mongoose.model("Review", reviewSchema);

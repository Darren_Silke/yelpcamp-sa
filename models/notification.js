var mongoose = require("mongoose");

var notificationSchema = new mongoose.Schema({
	username: String,
	campgroundSlug: String,
	createdAt: {
		type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model("Notification", notificationSchema);

var mongoose              = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var userSchema = new mongoose.Schema({
	username: String,
	password: String,
	firstName: {
		type: String,
		required: "First name cannot be blank"
	},
	lastName: {
		type: String,
		required: "Last name cannot be blank"
	},
	email: {
		type: String,
		unique: true,
		required: "Email cannot be blank"
	},
	resetPasswordToken: String,
	resetPasswordExpires: Date,
	isAdmin: {
		type: Boolean,
		default: false
	},
	notifications: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: "Notification"
		}
	],
	followers: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: "User"
		}
	]
});

userSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("User", userSchema);

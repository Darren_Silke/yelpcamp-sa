// Configure dotenv
require("dotenv").config();

// Require packages
var express        = require("express");
var app            = express();
var bodyParser     = require("body-parser");
var mongoose       = require("mongoose");
var flash          = require("connect-flash");
var passport       = require("passport");
var LocalStrategy  = require("passport-local");
var methodOverride = require("method-override");
var favicon        = require("serve-favicon");
var path           = require("path");
var session        = require("express-session");
var MemoryStore    = require("memorystore")(session);
var User           = require("./models/user");
var async          = require("async");

// Require routes
var userRoutes       = require("./routes/users");
var commentRoutes    = require("./routes/comments");
var reviewRoutes     = require("./routes/reviews");
var campgroundRoutes = require("./routes/campgrounds");
var indexRoutes      = require("./routes/index");

// Require Moment
app.locals.moment = require("moment");

// Configure environment
if (process.env.ENVIRONMENT === "production") {
	app.use((req, res, next) => {
		if (req.header("x-forwarded-proto") !== "https") {
			res.redirect(`https://${req.header("host")}${req.url}`);
		} else {
			next();
		}
	});
}

// Configure Mongoose for MongoDB
var url = process.env.DATABASE_URL || "mongodb://127.0.0.1:27017/yelpcamp-sa";
mongoose.connect(url, {
	useNewUrlParser: true,
	useFindAndModify: false,
	useCreateIndex: true,
	useUnifiedTopology: true
}, function(err){
	if (err) {
		console.error(err);
	} else {
		console.log("Connected to database!");
	}
});

// Configure view engine, serve-favicon, path, express, body-parser, method-override and connect-flash
app.set("view engine", "ejs");
app.use(favicon(path.join(__dirname, "public", "images", "favicon.ico")));
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));
app.use(flash());

// Configure express-session and memorystore
app.use(session({
	cookie: {maxAge: 86400000},
	store: new MemoryStore({
		checkPeriod: 86400000 // Prune expired entries every 24h
	}),
	secret: process.env.EXPRESS_SESSION_SECRET,
	resave: false,
	saveUninitialized: false
}));

// Configure Passport
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// Configure response local variables
app.use(async function(req, res, next){
	res.locals.currentUser = req.user;
	if (req.user) {
		try {
			// Sort the populated notifications array to show the latest first
			// Only populate the notifications array with 11 items
			var user = await User.findById(req.user._id).populate({
				path: "notifications",
				options: {
					sort: {createdAt: -1},
					limit: 11
				}
			}).exec();
			res.locals.notifications = user.notifications;
		} catch(err) {
			console.error(err);
		}
	}
	res.locals.error = req.flash("error");
	res.locals.success = req.flash("success");
	next();
});

// Configure routes
app.use("/", indexRoutes);
app.use("/campgrounds", campgroundRoutes);
app.use("/campgrounds/:slug/reviews", reviewRoutes);
app.use("/campgrounds/:slug/comments", commentRoutes);
app.use("/campgrounds/:slug/users", userRoutes);

// Configure server to listen on port and IP address
app.listen(process.env.PORT, process.env.IP, function(){
	console.log("The YelpCamp SA server has started!");
});
